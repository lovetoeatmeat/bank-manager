﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BankManage.Function_Explaination
{
    /// <summary>
    /// FunctionExplaination.xaml 的交互逻辑
    /// </summary>
    public partial class FunctionExplaination : Page
    {
        StreamReader streamReader;
        public FunctionExplaination()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button item = e.Source as Button;
            if (item != null)
            {
                string fileDirectory = Convert.ToString(Environment.CurrentDirectory);
                fileDirectory = fileDirectory.Substring(0, fileDirectory.IndexOf("bin"));
                //frame1.Source = new Uri(item.Tag.ToString(), UriKind.Relative);
                streamReader = new StreamReader(fileDirectory + "FunctionExplaination\\" + item.Tag.ToString() + ".txt");
                if (showTextBox.Text != "" && showTextBox.Text != null)
                {
                    showTextBox.Clear();
                }
                try
                {
                    while (streamReader.Peek() > -1)
                    {
                        showTextBox.Text += (streamReader.ReadLine() + "\n");
                    }
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }
    }

}

﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BankManage.employee
{
    /// <summary>
    /// Interaction logic for AddEmployee.xaml
    /// </summary>
    public partial class AddEmployee : Page
    {
        BankEntities context = new BankEntities();
        public AddEmployee()
        {
            InitializeComponent();
        }

        string photofilePath = "";
        //浏览照片
        private void buttonbrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == true)
            {
                photofilePath = ofd.FileName;
                //照片显示到image控件内
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.UriSource = new Uri(photofilePath, UriKind.RelativeOrAbsolute);
                bi.EndInit();
                this.imagePhoto.Source = bi;
            }
        }

        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            EmployeeInfo employee = new EmployeeInfo();
            employee.EmployeeNo = this.employeeNoTextBox.Text;
            employee.EmployeeName = this.employeeNameTextBox.Text;
            employee.sex = this.radioM.IsChecked == true ? "男" : "女";
            employee.workDate = this.datePickerWorkDate.SelectedDate;
            employee.idCard = this.idCardTextBox.Text;
            employee.telphone = this.telphoneTextBox.Text;
            employee.salary = Convert.ToDouble(this.SalaryTextBox.Text);

            //照片(读取照片内容到字节数组bt中）
            if (photofilePath != "")
            {
                Stream mystream = File.OpenRead(photofilePath);
                byte[] bt = new byte[mystream.Length];
                mystream.Read(bt, 0, (int)mystream.Length);
                employee.photo = bt;
            }
            //添加对象
            try
            {
                context.EmployeeInfo.Add(employee);
                int i = context.SaveChanges();
                MessageBox.Show(string.Format("成功添加{0}个员工信息", i));
            }
            catch
            {
                MessageBox.Show("添加员工失败！");
            }
            EmployeeBase page = new EmployeeBase();
            NavigationService ns = NavigationService.GetNavigationService(this);
            ns.Navigate(page);
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.employeeNoTextBox.Text = "";
            this.employeeNameTextBox.Text = "";
            this.idCardTextBox.Text = "";
            this.telphoneTextBox.Text = "";
            this.imagePhoto.Source = null;
            EmployeeBase page = new EmployeeBase();
            NavigationService ns = NavigationService.GetNavigationService(this);
            ns.Navigate(page);
        }
    }
}

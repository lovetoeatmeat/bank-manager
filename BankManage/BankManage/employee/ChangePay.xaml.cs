﻿using BankManage.money;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BankManage.employee
{
    /// <summary>
    /// ChangePay.xaml 的交互逻辑
    /// </summary>
    public partial class ChangePay : Page
    {
        BankEntities context = new BankEntities();
        public ChangePay()
        {
            InitializeComponent();
        }

        private void employeeNameTextBox_MouseLeave(object sender, MouseEventArgs e)
        {
            if (employeeNoTextBox.Text != "" && employeeNameTextBox.Text != "")
            {
                var q = from t in context.EmployeeInfo
                        where t.EmployeeNo == employeeNoTextBox.Text
                        select t;
                var p = q.Single();
                if (p.EmployeeName != employeeNameTextBox.Text)
                {
                    MessageBox.Show("员工号与员工姓名不匹配");
                    return;
                }

                //初始化员工工资
                employeeSalaryTextBox.Text = Convert.ToString(p.salary);
            }
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            var q = from t in context.EmployeeInfo
                        where t.EmployeeNo == employeeNoTextBox.Text
                        select t;
            var p = q.Single();
            if(p.EmployeeName!=employeeNameTextBox.Text)
            {
                MessageBox.Show("员工号与员工姓名不匹配");
                return;
            }

            if (raiseRadioButton.IsChecked == true)
            {
                try
                {
                    var query = from t in context.EmployeeInfo
                                where t.EmployeeNo == employeeNoTextBox.Text
                                select t;
                    foreach (var v in query)
                    {
                        v.salary += Convert.ToDouble(amountTextBox.Text);
                        employeeSalaryTextBox.Text = Convert.ToString(v.salary);
                        MessageBox.Show("工资调整成功，现在该员工的工资为 "+ Convert.ToString(v.salary));
                    }
                    context.SaveChanges();
                }
                catch
                {
                    MessageBox.Show("工资调整失败");
                }
            }

            if (cutRadioButton.IsChecked == true)
            {
                try
                {
                    var query = from t in context.EmployeeInfo
                                where t.EmployeeNo == employeeNoTextBox.Text
                                select t;
                    foreach (var v in query)
                    {
                        v.salary -= Convert.ToDouble(amountTextBox.Text);
                        employeeSalaryTextBox.Text = Convert.ToString(v.salary);
                        MessageBox.Show("工资调整成功，现在该员工的工资为 " + Convert.ToString(v.salary));
                    }
                    context.SaveChanges();
                }
                catch
                {
                    MessageBox.Show("工资调整失败");
                }
            }
            EmployeeBase page = new EmployeeBase();
            NavigationService ns = NavigationService.GetNavigationService(this);
            ns.Navigate(page);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            OperateRecord page = new OperateRecord();
            NavigationService ns = NavigationService.GetNavigationService(this);
            ns.Navigate(page);
        }

    }
}

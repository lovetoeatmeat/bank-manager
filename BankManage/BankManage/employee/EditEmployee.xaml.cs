﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BankManage.employee
{
    /// <summary>
    /// Interaction logic for EditEmployee.xaml
    /// </summary>
    public partial class EditEmployee : Page
    {
        BankEntities context = new BankEntities();
        EmployeeInfo editEmployee;
        public EditEmployee(string employeeNoID)
        {
            InitializeComponent();
            showEmployee(employeeNoID);
        }

        public void showEmployee(string id)
        {
            var q = from t in context.EmployeeInfo
                    where t.EmployeeNo == id
                    select t;
            if (q != null)
            {
                editEmployee = q.FirstOrDefault();
                this.employeeNoTextBox.Text = id;
                this.employeeNameTextBox.Text = editEmployee.EmployeeName;
                if (editEmployee.sex == "男")
                    this.radioM.IsChecked = true;
                else
                    this.radioF.IsChecked = true;
                this.datePickerWorkDate.SelectedDate = editEmployee.workDate;
                this.idCardTextBox.Text = editEmployee.idCard;
                this.telphoneTextBox.Text = editEmployee.telphone;
                //显示照片
                if (editEmployee.photo != null)
                {
                    MemoryStream ms = new MemoryStream(editEmployee.photo);
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    bi.StreamSource = ms;
                    bi.EndInit();
                    this.imagePhoto.Source = bi;
                }
            }
        }

        string photofilePath = "";
        //浏览照片
        private void buttonbrowse_Click(object sender, RoutedEventArgs e)
        {
            //清空image控件内的图片
            this.imagePhoto.Source = null;
            //打开对话框选择图像
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == true)
            {
                photofilePath = ofd.FileName;
                //图像显示到image控件内
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.UriSource = new Uri(photofilePath, UriKind.RelativeOrAbsolute);
                bi.EndInit();
                this.imagePhoto.Source = bi;
            }
        }

        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            editEmployee.EmployeeNo = this.employeeNoTextBox.Text;
            editEmployee.EmployeeName = this.employeeNameTextBox.Text;
            editEmployee.sex = this.radioM.IsChecked == true ? "男" : "女";
            editEmployee.workDate = this.datePickerWorkDate.SelectedDate;
            editEmployee.idCard = this.idCardTextBox.Text;
            editEmployee.telphone = this.telphoneTextBox.Text;
            //照片(读取照片内容到字节数组bt中）
            if (photofilePath != "")
            {
                Stream mystream = File.OpenRead(photofilePath);
                byte[] bt = new byte[mystream.Length];
                mystream.Read(bt, 0, (int)mystream.Length);
                editEmployee.photo = bt;
            }
            //保存对象的修改
            try
            {
                int i = context.SaveChanges();
                MessageBox.Show(string.Format("成功修改{0}个员工信息", i));
            }
            catch
            {
                MessageBox.Show("修改员工失败！");
            }
            EmployeeBase page = new EmployeeBase();
            NavigationService ns = NavigationService.GetNavigationService(this);
            ns.Navigate(page);
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.employeeNoTextBox.Text = "";
            this.employeeNameTextBox.Text = "";
            this.idCardTextBox.Text = "";
            this.telphoneTextBox.Text = "";
            this.imagePhoto.Source = null;
            EmployeeBase page = new EmployeeBase();
            NavigationService ns = NavigationService.GetNavigationService(this);
            ns.Navigate(page);
        }
    }
}

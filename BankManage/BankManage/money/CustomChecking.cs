﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankManage.common;

namespace BankManage
{
    public class CustomChecking : Custom
    {
        /// 开户
        /// </summary>
        /// <param name="accountNumber">帐号</param>
        /// <param name="money">开户金额</param>
        public override void Create(string accountNumber, double money)
        {
            base.Create(accountNumber, money);
            base.Deposit(accountNumber, "结息", DataOperation.GetRate(RateType.活期) * money);
        }

        /// <summary>
        ///存款 
        /// </summary>
        public override void Deposit(string accountNumber, string genType, double money)
        {
            base.Deposit(accountNumber, "存款", money);
            //结算利息
            base.Deposit(accountNumber, "结息", DataOperation.GetRate(RateType.活期) * money);
        }

        /// <summary>
        ///取款 
        /// </summary>
        /// <param name="money">取款金额</param>
        public override void Withdraw(string accountNumber, double money)
        {
            if (!ValidBeforeWithdraw(money)) return;
            //取款
            base.Withdraw(accountNumber, money);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankManage.common;

namespace BankManage
{
    public class CustomDeposit_in_Installment_While_Withdraw_in_Lump_Sum : Custom
    {
        public RateType type { get; set; }
        public double getTime()
        {
            TimeSpan ts = DateTime.Now - DateTime.Parse("1970-1-1");
            double timeStamp = Convert.ToDouble(ts.TotalSeconds);
            return timeStamp;
        }

        /// 开户
        /// </summary>
        /// <param name="accountNumber">帐号</param>
        /// <param name="money">开户金额</param>
        public override void Create(string accountNumber, double money)
        {
            base.Create(accountNumber, money);
        }

        /// <summary>
        ///存款 
        /// </summary>
        static double depositTime;
        public override void Deposit(string accountNumber, string genType, double money)
        {
            base.Deposit(accountNumber, "存款", money);
            depositTime = getTime();
        }

        /// <summary>
        ///取款 
        /// </summary>
        /// <param name="money">取款金额</param>
        static double withdrawTime;
        public override void Withdraw(string accountNumber, double money)
        {
            if (!ValidBeforeWithdraw(money)) return;
            withdrawTime = getTime();
            //结算利息
            if (DataOperation.GetCustom(accountNumber).AccountInfo.accountYearType == "存期1年")
            {
                if (DataOperation.GetCustom(accountNumber).AccountInfo.depositTimes == 12)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestDeposit_in_Installment_While_Withdraw_in_Lump_Sum.oneYearDeposit_in_Installment_While_Withdraw_in_Lump_Sum(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
                else if (DataOperation.GetCustom(accountNumber).AccountInfo.depositTimes < 12)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestDeposit_in_Installment_While_Withdraw_in_Lump_Sum.advanceDeposit_in_Installment_While_Withdraw_in_Lump_Sum(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
                else if (DataOperation.GetCustom(accountNumber).AccountInfo.depositTimes > 12)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestDeposit_in_Installment_While_Withdraw_in_Lump_Sum.overDeposit_in_Installment_While_Withdraw_in_Lump_Sum(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
            }

            if (DataOperation.GetCustom(accountNumber).AccountInfo.accountYearType == "存期3年")
            {
                if (DataOperation.GetCustom(accountNumber).AccountInfo.depositTimes == 36)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestDeposit_in_Installment_While_Withdraw_in_Lump_Sum.threeYearDeposit_in_Installment_While_Withdraw_in_Lump_Sum(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
                else if (DataOperation.GetCustom(accountNumber).AccountInfo.depositTimes < 36)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestDeposit_in_Installment_While_Withdraw_in_Lump_Sum.advanceDeposit_in_Installment_While_Withdraw_in_Lump_Sum(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
                else if (DataOperation.GetCustom(accountNumber).AccountInfo.depositTimes > 36)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestDeposit_in_Installment_While_Withdraw_in_Lump_Sum.overDeposit_in_Installment_While_Withdraw_in_Lump_Sum(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
            }

            if (DataOperation.GetCustom(accountNumber).AccountInfo.accountYearType == "存期5年")
            {
                if (DataOperation.GetCustom(accountNumber).AccountInfo.depositTimes == 60)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestDeposit_in_Installment_While_Withdraw_in_Lump_Sum.fiveYearDeposit_in_Installment_While_Withdraw_in_Lump_Sum(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
                else if (DataOperation.GetCustom(accountNumber).AccountInfo.depositTimes < 60)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestDeposit_in_Installment_While_Withdraw_in_Lump_Sum.advanceDeposit_in_Installment_While_Withdraw_in_Lump_Sum(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
                else if (DataOperation.GetCustom(accountNumber).AccountInfo.depositTimes > 60)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestDeposit_in_Installment_While_Withdraw_in_Lump_Sum.overDeposit_in_Installment_While_Withdraw_in_Lump_Sum(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
            }

        }
    }
}

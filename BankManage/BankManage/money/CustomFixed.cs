﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BankManage.common;

namespace BankManage
{
    /// <summary>
    /// 定期存款
    /// </summary>
    public class CustomFixed : Custom
    {
        public RateType type { get; set; }
        public double getTime()
        {
            TimeSpan ts = DateTime.Now - DateTime.Parse("1970-1-1");
            double timeStamp = Convert.ToDouble(ts.TotalSeconds);
            return timeStamp;
        }

        /// 开户
        /// </summary>
        /// <param name="accountNumber">帐号</param>
        /// <param name="money">开户金额</param>
        public override void Create(string accountNumber,double money)
        {
            base.Create(accountNumber, money);
        }

        /// <summary>
        ///存款 
        /// </summary>
        static double depositTime;
        public override void Deposit(string accountNumber, string genType,double money)
        {
            base.Deposit(accountNumber, "存款", money);
            depositTime = getTime();
        }

        /// <summary>
        ///取款 
        /// </summary>
        /// <param name="money">取款金额</param>
        static double withdrawTime;
        public override void Withdraw(string accountNumber, double money)
        {
            if (!ValidBeforeWithdraw(money)) return;
            withdrawTime = getTime();
            //结算利息
            if (DataOperation.GetCustom(accountNumber).AccountInfo.accountYearType == "存期1年")
            {
                if (Math.Round((withdrawTime - depositTime) / (365 * 24 * 60 * 60), 2) == 1.00)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestFixed.oneYearFixed(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
                else if (Math.Round((withdrawTime - depositTime) / (365 * 24 * 60 * 60), 2) < 1.00)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestFixed.advanceFixed(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
                else if(Math.Round((withdrawTime - depositTime) / (365 * 24 * 60 * 60), 2) > 1.00)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestFixed.overFixed(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
            }

            if (DataOperation.GetCustom(accountNumber).AccountInfo.accountYearType == "存期3年")
            {
                if (Math.Round((withdrawTime - depositTime) / (365 * 24 * 60 * 60), 2) == 3.00)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestFixed.threeYearFixed(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
                else if (Math.Round((withdrawTime - depositTime) / (365 * 24 * 60 * 60), 2) < 3.00)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestFixed.advanceFixed(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
                else if (Math.Round((withdrawTime - depositTime) / (365 * 24 * 60 * 60), 2) > 3.00)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestFixed.overFixed(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
            }

            if (DataOperation.GetCustom(accountNumber).AccountInfo.accountYearType == "存期5年")
            {
                if (Math.Round((withdrawTime - depositTime) / (365 * 24 * 60 * 60), 2) == 5.00)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestFixed.fiveYearFixed(accountNumber, AccountBalance));
                    //取款
                    base.Withdraw(accountNumber, money);
                }
                else if (Math.Round((withdrawTime - depositTime) / (365 * 24 * 60 * 60), 2) < 5.00)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestFixed.advanceFixed(accountNumber, AccountBalance));
    
                    //取款
                    base.Withdraw(accountNumber, money);
                }
                else if (Math.Round((withdrawTime - depositTime) / (365 * 24 * 60 * 60), 2) > 5.00)
                {
                    //计算利息
                    base.Deposit(accountNumber, "结息", interestFixed.overFixed(accountNumber, AccountBalance));
                    //double interest = interestFixed.overFixed(accountNumber, AccountBalance);
                    //AccountBalance += interest;
                    //取款
                    base.Withdraw(accountNumber, money);
                }
            }
            
        }
    }
}

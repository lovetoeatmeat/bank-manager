﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BankManage.common;

namespace BankManage.money
{
    /// <summary>
    /// Deposit.xaml 的交互逻辑
    /// </summary>
    public partial class Deposit : Page
    {
        BankEntities context = new BankEntities();
        public Deposit()
        {
            InitializeComponent();
            InitComboBox();
        }
        private void InitComboBox()
        {
            string[] items = Enum.GetNames(typeof(YearType));
            foreach (var s in items)
            {
                yearTypeComboBox.Items.Add(s);
            }
            yearTypeComboBox.SelectedIndex = 0;
        }
        //存款
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            Custom custom = DataOperation.GetCustom(this.txtAccount.Text);
            if (custom == null)
            {
                MessageBox.Show("帐号不存在！");
                return;
            }
            custom.MoneyInfo.accountNo = txtAccount.Text;
            if (DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.accountType == "活期存款")
            {
                if (Convert.ToDouble(txtmount.Text) < 100)
                {
                    MessageBox.Show("抱歉，最低起存金额为100");
                    return;
                }
            }

            if (DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.accountType == "定期存款")
            {
                if(DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.depositTimes>0)
                {
                    MessageBox.Show("抱歉，定期存款用户只允许单次存款");
                    return;
                }
                if (Convert.ToDouble(txtmount.Text) < 100)
                {
                    MessageBox.Show("抱歉，最低起存金额为100");
                    return;
                }
            }

            if (DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.accountType == "零存整取")
            {
                if(yearTypeComboBox.Text== "存期1年")
                {
                    if (DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.depositTimes == 12)
                    {
                        MessageBox.Show("抱歉，存款期限为1年的零存整取用户只允许存款12次");
                        return;
                    }
                }
                if (yearTypeComboBox.Text == "存期3年")
                {
                    if (DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.depositTimes == 36)
                    {
                        MessageBox.Show("抱歉，存款期限为3年的零存整取用户只允许存款36次");
                        return;
                    }
                }
                if (yearTypeComboBox.Text == "存期5年")
                {
                    if (DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.depositTimes == 60)
                    {
                        MessageBox.Show("抱歉，存款期限为5年的零存整取用户只允许存款60次");
                        return;
                    }
                }
                if (Convert.ToDouble(txtmount.Text) < 5)
                {
                    MessageBox.Show("抱歉，最低起存金额为5");
                    return;
                }
            }

            if (DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.accountType == "定期存款"|| DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.accountType == "零存整取")
            {
                var q = from t in context.AccountInfo
                        where t.accountNo == this.txtAccount.Text
                        select t;
                foreach(var v in q)
                {
                    v.accountYearType = yearTypeComboBox.Text;
                    v.depositTimes += 1;
                }
                if(DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.accountType == "零存整取")
                {
                    var p = from t in context.AccountInfo
                            where t.accountNo == this.txtAccount.Text
                            select t;
                    foreach (var v in p)
                    {
                        v.installmentAmount = Convert.ToDouble(installmentTextBox.Text);
                    }
                }
                context.SaveChanges();
            }
            custom.Deposit(this.txtAccount.Text, "存款", double.Parse(this.txtmount.Text));
            OperateRecord page = new OperateRecord();
            NavigationService ns = NavigationService.GetNavigationService(this);
            ns.Navigate(page);
        }
        //取消存款
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            OperateRecord page = new OperateRecord();
            NavigationService ns = NavigationService.GetNavigationService(this);
            ns.Navigate(page);
        }

        private void txtAccount_MouseLeave(object sender, MouseEventArgs e)
        {
            Custom custom = DataOperation.GetCustom(this.txtAccount.Text);
            if (this.txtAccount.Text != "" && DataOperation.GetCustom(this.txtAccount.Text) != null)
            {
                if (DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.accountType != "定期存款" && DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.accountType != "零存整取")
                {
                    yearTypeComboBox.Items.Clear();
                    string str = "只有定期存款用户和零存整取用户需要填写";
                    yearTypeComboBox.Items.Add(str);
                    yearTypeComboBox.SelectedIndex = 0;
                    yearTypeComboBox.IsEnabled = false;
                }
                if (DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.accountType != "零存整取")
                {
                    string str = "只有零存整取用户需要填写";
                    installmentTextBox.Text = str;
                    installmentTextBox.IsReadOnly = true;
                }
                if (DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.accountType == "零存整取" && DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.depositTimes > 0)
                {
                    yearTypeComboBox.Text = DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.accountYearType;
                    yearTypeComboBox.IsEnabled = false;
                    installmentTextBox.Text = Convert.ToString(DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.installmentAmount);
                    txtmount.Text = Convert.ToString(DataOperation.GetCustom(this.txtAccount.Text).AccountInfo.installmentAmount);
                    installmentTextBox.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                    txtmount.Foreground = new SolidColorBrush(Color.FromRgb(255, 0, 0));
                    installmentTextBox.IsEnabled = false;
                    txtmount.IsEnabled = false;
                }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankManage.common;

namespace BankManage
{
    class interestDeposit_in_Installment_While_Withdraw_in_Lump_Sum
    {
        static double interest;
        static public double oneYearDeposit_in_Installment_While_Withdraw_in_Lump_Sum(string accountNumber, double money)
        {
            interest = DataOperation.GetRate(RateType.零存整取1年) * money;
            return interest;
        }

        static public double threeYearDeposit_in_Installment_While_Withdraw_in_Lump_Sum(string accountNumber, double money)
        {
            interest = DataOperation.GetRate(RateType.零存整取3年) * money;
            return interest;
        }

        static public double fiveYearDeposit_in_Installment_While_Withdraw_in_Lump_Sum(string accountNumber, double money)
        {
            interest = DataOperation.GetRate(RateType.零存整取5年) * money;
            return interest;
        }

        static public double overDeposit_in_Installment_While_Withdraw_in_Lump_Sum(string accountNumber, double money)
        {
            if (DataOperation.GetCustom(accountNumber).AccountInfo.accountYearType == "存期1年")
            {
                interest = DataOperation.GetRate(RateType.零存整取超期部分) * (money + (DataOperation.GetRate(RateType.零存整取1年) * money));
            }
            if (DataOperation.GetCustom(accountNumber).AccountInfo.accountYearType == "存期3年")
            {
                interest = DataOperation.GetRate(RateType.零存整取超期部分) * (money + (DataOperation.GetRate(RateType.零存整取3年) * money));
            }
            if (DataOperation.GetCustom(accountNumber).AccountInfo.accountYearType == "存期5年")
            {
                interest = DataOperation.GetRate(RateType.零存整取超期部分) * (money + (DataOperation.GetRate(RateType.零存整取5年) * money));
            }
            return interest;
        }

        static public double advanceDeposit_in_Installment_While_Withdraw_in_Lump_Sum(string accountNumber, double money)
        {
            interest = DataOperation.GetRate(RateType.零存整取违规) * money;
            return interest;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankManage.common;

namespace BankManage
{
    public class interestFixed
    {
        static double interest;
        static public double oneYearFixed(string accountNumber, double money)
        {
            interest=DataOperation.GetRate(RateType.定期1年) * money;
            return interest;
        }

        static public double threeYearFixed(string accountNumber, double money)
        {
            interest = DataOperation.GetRate(RateType.定期3年) * money;
            return interest;
        }

        static public double fiveYearFixed(string accountNumber, double money)
        {
            interest=DataOperation.GetRate(RateType.定期5年) * money;
            return interest;
        }

        static public double overFixed(string accountNumber, double money)
        {
            if (DataOperation.GetCustom(accountNumber).AccountInfo.accountYearType == "存期1年")
            {
                interest = DataOperation.GetRate(RateType.定期超期部分) * (money + (DataOperation.GetRate(RateType.定期1年) * money));
            }
            if (DataOperation.GetCustom(accountNumber).AccountInfo.accountYearType == "存期3年")
            {
                interest = DataOperation.GetRate(RateType.定期超期部分) * (money + (DataOperation.GetRate(RateType.定期3年) * money));
            }
            if (DataOperation.GetCustom(accountNumber).AccountInfo.accountYearType == "存期5年")
            {
                interest = DataOperation.GetRate(RateType.定期超期部分) * (money + (DataOperation.GetRate(RateType.定期5年) * money));
            }
            return interest;
        }

        static public double advanceFixed(string accountNumber, double money)
        {
            interest = DataOperation.GetRate(RateType.定期提前支取) * money;
            return interest;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankManage.other.lottery
{
    class LotteryTicket
    {
        string[] Red = new string[6];
        string Blue;
        string[] rRed = new string[6];
        string rBlue;
        Random r = new Random();

        public LotteryTicket(string[] red, string blue)
        {
            for (int i = 0; i < Red.Length; i++)
            {
                Red[i] = red[i];
            }
            Blue = blue;
        }

        public void generate()
        {
            for (int i = 0; i < rRed.Length; i++)
            {
                rRed[i] = Convert.ToString(r.Next(1, 34));
            }
            rBlue = Convert.ToString(r.Next(1, 17));
        }

        public int Draw()
        {
            int count = 0;
            this.generate();
            for (int i = 0; i < Red.Length; i++)
            {
                if (Red[i] == rRed[i])
                {
                    count++;
                }
            }
            if (count==6&&Blue==rBlue)
            {
                return 20000;
            }
            if (count==6)
            {
                return 6000;
            }
            if (count==5 && Blue==rBlue)
            {
                return 2000;
            }
            if (count==5)
            {
                return 500;
            }
            if (count==4 || count==3 && Blue == rBlue)
            {
                return 100;
            }
            else
            {
                return 0;
            }
        }
    }
}

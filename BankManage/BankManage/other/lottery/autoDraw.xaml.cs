﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace BankManage.other.lottery
{
    /// <summary>
    /// Interaction logic for autoDraw.xaml
    /// </summary>
    public partial class autoDraw : Page
    {
        public autoDraw()
        {
            InitializeComponent();
        }

        private void autoDrawConfirmButton_Click(object sender, RoutedEventArgs e)
        {

        }

        DispatcherTimer timer = new DispatcherTimer();
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            timer.Tick += Timer_Tick;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (weeklyRadioButton.IsChecked == true)
            {
                timer.Interval = TimeSpan.FromDays(7);
            }
            if (monthlyRadioButton.IsChecked == true)
            {
                timer.Interval = TimeSpan.FromDays(30);
            }
            if (monthlyRadioButton.IsChecked == true)
            {
                timer.Interval = TimeSpan.FromDays(90);
            }
            if (monthlyRadioButton.IsChecked == true)
            {
                timer.Interval = TimeSpan.FromDays(365);
            }
        }

        private void weeklyRadioButton_Checked(object sender, RoutedEventArgs e)
        {

        }
    }
}

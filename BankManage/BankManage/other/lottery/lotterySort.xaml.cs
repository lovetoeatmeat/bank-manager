﻿using BankManage.money;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BankManage.other.lottery
{
    /// <summary>
    /// Interaction logic for lotterySort.xaml
    /// </summary>
    public partial class lotterySort : Page
    {
        public lotterySort()
        {
            InitializeComponent();
        }

        private void raffleButton_Click(object sender, RoutedEventArgs e)
        {
            raffle rf = new raffle();
            raffle page = new raffle();
            NavigationService ns = NavigationService.GetNavigationService(this);
            ns.Navigate(page);
        }

        private void lotteryTicketButton_Click(object sender, RoutedEventArgs e)
        {
            lotteryTicket lt = new lotteryTicket();
            lotteryTicket page = new lotteryTicket();
            NavigationService ns = NavigationService.GetNavigationService(this);
            ns.Navigate(page);
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            OperateRecord page = new OperateRecord();
            NavigationService ns = NavigationService.GetNavigationService(this);
            ns.Navigate(page);
        }
    }
}

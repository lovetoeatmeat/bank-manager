﻿using BankManage.common;
using BankManage.other.transfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BankManage.other.lottery
{
    /// <summary>
    /// Interaction logic for raffle.xaml
    /// </summary>
    public partial class raffle : Page
    {
        BankEntities context = new BankEntities();
        public raffle()
        {
            InitializeComponent();
        }

        private void raffleDrawButton_Click(object sender, RoutedEventArgs e)
        {
            if (DataOperation.GetCustom(txtAccount.Text).AccountInfo.accountType == "定期存款" || DataOperation.GetCustom(txtAccount.Text).AccountInfo.accountType == "零存整取")
            {
                MessageBox.Show("抱歉，仅支持活期存款用户");
                return;
            }

            Custom custom = DataOperation.GetCustom(txtAccount.Text);
            if (custom.AccountInfo.accountPass != txtPassword.Password)
            {
                MessageBox.Show("密码不正确");
                return;
            }
            if (custom.AccountBalance < 3)
            {
                MessageBox.Show("抱歉，账户余额不足");
                return;
            }
            //每次click button从余额中扣款
            Transfer.transferBetweenAccounts(txtAccount.Text, "000001", 3);
            Raffle rf = new Raffle();
            double awards = rf.Draw();
            this.awardsTextBox.Text = "您的中奖金额为 " + Convert.ToString(awards);
            if(awards>0)
            {
                MessageBox.Show("恭喜，您的中奖金额为 " + Convert.ToString(awards));
            }
            else
            {
                MessageBox.Show("很遗憾，本次您未能中奖");
            }
            //每次中奖向余额中增加奖金数
            Transfer.transferFromjackpot(txtAccount.Text, awards);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            lotterySort page = new lotterySort();
            NavigationService ns = NavigationService.GetNavigationService(this);
            ns.Navigate(page);
        }
    }
}

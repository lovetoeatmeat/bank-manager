﻿using BankManage.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankManage.other.transfer
{
    class Transfer
    {
        static public MoneyInfo MoneyInfo { get; set; }
        static public void transferBetweenAccounts(string transferAccountNumber, string acceptAccountNumber, double money)
        {
            BankEntities context = new BankEntities();
            var q1 = from t1 in context.AccountInfo
                     where t1.accountNo == transferAccountNumber
                     select t1;
            foreach (var v1 in q1)
            {
                v1.accountBalance -= money;
            }
            context.SaveChanges();
            InsertData(transferAccountNumber, "转账支付", -money);
            var q2 = from t2 in context.AccountInfo
                     where t2.accountNo == acceptAccountNumber
                     select t2;
            foreach (var v2 in q2)
            {
                v2.accountBalance += money;
            }
            context.SaveChanges();
            InsertData(acceptAccountNumber, "转账汇入", money);
            //context.SaveChanges();
        }

        static public void transferFromjackpot(string acceptAccountNumber, double money)
        {
            BankEntities context = new BankEntities();
            if (money > 0)
            {
                transferBetweenAccounts("000001", acceptAccountNumber, money);
            }
        }

        static public void InsertData(string accountNumber, string genType, double money)
        {
            BankEntities context = new BankEntities();
            MoneyInfo = new MoneyInfo();
            MoneyInfo.accountNo = accountNumber;
            MoneyInfo.dealDate = DateTime.Now;
            MoneyInfo.dealType = genType;
            MoneyInfo.dealMoney = money;
            MoneyInfo.balance = (double)DataOperation.GetCustom(accountNumber).AccountInfo.accountBalance;
            context.MoneyInfo.Add(MoneyInfo);
            context.SaveChanges();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BankManage.common;
using BankManage.money;

namespace BankManage.other.transfer
{
    /// <summary>
    /// Interaction logic for transfer.xaml
    /// </summary>
    public partial class transfer : Page
    {
        public transfer()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (DataOperation.GetCustom(transferAccountTextBox.Text).AccountInfo.accountType == "定期存款" || DataOperation.GetCustom(transferAccountTextBox.Text).AccountInfo.accountType == "零存整取"|| DataOperation.GetCustom(acceptAccountTextBox.Text).AccountInfo.accountType == "定期存款" || DataOperation.GetCustom(acceptAccountTextBox.Text).AccountInfo.accountType == "零存整取")
            {
                MessageBox.Show("抱歉，转账服务仅支持在活期存款用户间进行");
                return;
            }

            Custom custom = DataOperation.GetCustom(transferAccountTextBox.Text);
            if (custom.AccountInfo.accountPass != transferAccountPasswordBox.Password)
            {
                MessageBox.Show("密码不正确");
                return;
            }

            if (!ValidBeforeTransfer(Convert.ToDouble(txtmount.Text)))
            {
                return;
            }
            Transfer.transferBetweenAccounts(transferAccountTextBox.Text, acceptAccountTextBox.Text, Convert.ToDouble(txtmount.Text));
            MessageBox.Show("转账成功，现在您的账户余额为 " + (custom.AccountInfo.accountBalance - Convert.ToDouble(txtmount.Text)));
        }

        public bool ValidBeforeTransfer(double money)
        {
            if (money <= 0)
            {
                MessageBox.Show("转账金额不能为零或负值");
                return false;
            }
            if (money > DataOperation.GetCustom(transferAccountTextBox.Text).AccountInfo.accountBalance)
            {
                MessageBox.Show("转账金额不能比余额大");
                return false;
            }
            return true;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            OperateRecord page = new OperateRecord();
            NavigationService ns = NavigationService.GetNavigationService(this);
            ns.Navigate(page);
        }
    }
}

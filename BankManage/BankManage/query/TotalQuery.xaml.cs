﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BankManage.query
{
    /// <summary>
    /// TotalQuery.xaml 的交互逻辑
    /// </summary>
    public partial class TotalQuery : Page
    {
        BankEntities context = new BankEntities();
        public TotalQuery()
        {
            InitializeComponent();
            this.Unloaded += TotalQuery_Unloaded;
        }

        void TotalQuery_Unloaded(object sender, RoutedEventArgs e)
        {
            context.Dispose();
        }
        //查询当前账号的所有记录信息
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            /*var query = from t in context.MoneyInfo
                        where t.accountNo == txtID.Text
                        select t;
            datagrid1.ItemsSource = query.ToList();*/

            string[] strDealDate = dealDateTextBox.Text.Split('-');
            int integerDealDateYear = 0;
            int integerDealDateMonth = 0;
            int integerDealDateDay = 0;
            double DealMoney = 0;
            if (dealDateTextBox.Text != "" && dealDateTextBox.Text != null)
            {
                integerDealDateYear = Convert.ToInt32(strDealDate[0]);
                integerDealDateMonth = Convert.ToInt32(strDealDate[1]);
                integerDealDateDay = Convert.ToInt32(strDealDate[2]);
            }
            if (dealMoneyTextBox.Text != "" && dealMoneyTextBox.Text != null)
            {
                DealMoney = Convert.ToDouble(dealMoneyTextBox.Text);
            }

            if (accountNumberCheckBox.IsChecked == true && dealDateCheckBox.IsChecked == true && dealTypeCheckBox.IsChecked == true && dealMoneyCheckBox.IsChecked == true)
            {
                var query = from t in context.MoneyInfo
                            where t.accountNo == txtID.Text && t.dealType == dealTypeTextBox.Text && t.dealMoney == DealMoney && t.dealDate.Year == integerDealDateYear && t.dealDate.Month == integerDealDateMonth && t.dealDate.Day == integerDealDateDay
                            select t;
                datagrid1.ItemsSource = query.ToList();
            }

            else if (accountNumberCheckBox.IsChecked == true && dealDateCheckBox.IsChecked == true && dealTypeCheckBox.IsChecked == true)
            {
                var query = from t in context.MoneyInfo
                            where t.accountNo == txtID.Text && t.dealType == dealTypeTextBox.Text && t.dealDate.Year == integerDealDateYear && t.dealDate.Month == integerDealDateMonth && t.dealDate.Day == integerDealDateDay
                            select t;
                datagrid1.ItemsSource = query.ToList();
            }

            else if (accountNumberCheckBox.IsChecked == true && dealDateCheckBox.IsChecked == true && dealMoneyCheckBox.IsChecked == true)
            {
                var query = from t in context.MoneyInfo
                            where t.accountNo == txtID.Text && t.dealMoney == DealMoney && t.dealDate.Year == integerDealDateYear && t.dealDate.Month == integerDealDateMonth && t.dealDate.Day == integerDealDateDay
                            select t;
                datagrid1.ItemsSource = query.ToList();
            }

            else if (accountNumberCheckBox.IsChecked == true && dealTypeCheckBox.IsChecked == true && dealMoneyCheckBox.IsChecked == true)
            {
                var query = from t in context.MoneyInfo
                            where t.accountNo == txtID.Text && t.dealType == dealTypeTextBox.Text && t.dealMoney == DealMoney
                            select t;
                datagrid1.ItemsSource = query.ToList();
            }

            else if (dealDateCheckBox.IsChecked == true && dealTypeCheckBox.IsChecked == true && dealMoneyCheckBox.IsChecked == true)
            {
                var query = from t in context.MoneyInfo
                            where t.dealType == dealTypeTextBox.Text && t.dealMoney == DealMoney && t.dealDate.Year == integerDealDateYear && t.dealDate.Month == integerDealDateMonth && t.dealDate.Day == integerDealDateDay
                            select t;
                datagrid1.ItemsSource = query.ToList();
            }

            else if (accountNumberCheckBox.IsChecked == true && dealDateCheckBox.IsChecked == true)
            {
                var query = from t in context.MoneyInfo
                            where t.accountNo == txtID.Text && t.dealDate.Year == integerDealDateYear && t.dealDate.Month == integerDealDateMonth && t.dealDate.Day == integerDealDateDay
                            select t;
                datagrid1.ItemsSource = query.ToList();
            }

            else if (accountNumberCheckBox.IsChecked == true && dealTypeCheckBox.IsChecked == true)
            {
                var query = from t in context.MoneyInfo
                            where t.accountNo == txtID.Text && t.dealType == dealTypeTextBox.Text
                            select t;
                datagrid1.ItemsSource = query.ToList();
            }

            else if (accountNumberCheckBox.IsChecked == true && dealMoneyCheckBox.IsChecked == true)
            {
                var query = from t in context.MoneyInfo
                            where t.accountNo == txtID.Text && t.dealMoney == DealMoney
                            select t;
                datagrid1.ItemsSource = query.ToList();
            }

            else if (dealDateCheckBox.IsChecked == true && dealTypeCheckBox.IsChecked == true)
            {
                var query = from t in context.MoneyInfo
                            where t.dealType == dealTypeTextBox.Text && t.dealDate.Year == integerDealDateYear && t.dealDate.Month == integerDealDateMonth && t.dealDate.Day == integerDealDateDay
                            select t;
                datagrid1.ItemsSource = query.ToList();
            }

            else if (dealDateCheckBox.IsChecked == true && dealMoneyCheckBox.IsChecked == true)
            {
                var query = from t in context.MoneyInfo
                            where t.dealMoney == DealMoney && t.dealDate.Year == integerDealDateYear && t.dealDate.Month == integerDealDateMonth && t.dealDate.Day == integerDealDateDay
                            select t;
                datagrid1.ItemsSource = query.ToList();
            }

            else if (dealTypeCheckBox.IsChecked == true && dealMoneyCheckBox.IsChecked == true)
            {
                var query = from t in context.MoneyInfo
                            where t.dealType == dealTypeTextBox.Text && t.dealMoney == DealMoney
                            select t;
                datagrid1.ItemsSource = query.ToList();
            }

            else if (accountNumberCheckBox.IsChecked == true)
            {
                var query = from t in context.MoneyInfo
                            where t.accountNo == txtID.Text
                            select t;
                datagrid1.ItemsSource = query.ToList();
            }

            else if (dealDateCheckBox.IsChecked == true)
            {
                var query = from t in context.MoneyInfo
                            where t.dealDate.Year == integerDealDateYear && t.dealDate.Month == integerDealDateMonth && t.dealDate.Day == integerDealDateDay
                            select t;
                datagrid1.ItemsSource = query.ToList();
            }

            else if (dealTypeCheckBox.IsChecked == true)
            {
                var query = from t in context.MoneyInfo
                            where t.dealType == dealTypeTextBox.Text
                            select t;
                datagrid1.ItemsSource = query.ToList();
            }

            else if (dealMoneyCheckBox.IsChecked == true)
            {
                var query = from t in context.MoneyInfo
                            where t.dealMoney == DealMoney
                            select t;
                datagrid1.ItemsSource = query.ToList();
            }


            /*bool moreFactors = false;
            var db = context.Database;
            string stringSql = "select *from MoneyInfo";
            if(accountNumberCheckBox.IsChecked==true)
            {
                stringSql += "where accountNo=" + txtID.Text + " ";
                moreFactors = true;
            }
            if(dealDateCheckBox.IsChecked==true)
            {
                if(moreFactors)
                {
                    stringSql += "and dealDate=" + dealDateTextBox.Text + " ";
                }
                else
                {
                    stringSql += "where dealDate=" + dealDateTextBox.Text + " ";
                    moreFactors = true;
                }
            }
            if (dealTypeCheckBox.IsChecked == true)
            {
                if (moreFactors)
                {
                    stringSql += "and dealType=" + dealTypeTextBox.Text + " ";
                }
                else
                {
                    stringSql += "where dealType=" + dealTypeTextBox.Text + " ";
                    moreFactors = true;
                }
            }
            if (dealMoneyCheckBox.IsChecked == true)
            {
                if (moreFactors)
                {
                    stringSql += "and dealMoney=" + dealMoneyTextBox.Text + " ";
                }
                else
                {
                    stringSql += "where dealMoney=" + dealMoneyTextBox.Text + " ";
                    moreFactors = true;
                }
            }
            db.ExecuteSqlCommand(stringSql);*/
        }
    }
}

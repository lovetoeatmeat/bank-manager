1. # 银行管理系统

   #### 介绍

   [基于C#的银行管理系统](https://gitee.com/lovetoeatmeat/bank-manager.git)，c#编写，主要涵盖存取款模块，汇总查询模块，职员管理模块，扩展功能模块，页面设计模块五个模块。

   主界面

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-0.1a.jpg)![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-0.1b.jpg)![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-0.2a.jpg)![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-0.2b.jpg)

   ##### 存取款模块

   主要实现的功能：
   （1）开户：a.对账户的账号，账户名，身份证号，密码，开户类型进行录入，直接保存至AccountInfo表中，开户信息直接显示在操作记录窗口。
             b.对不同开户类型设置了开户最低金额，在按下“确定”按钮后会对开户类型进行检测，低于相应的开户金额则弹出对话框进行提示。
             c.对开户账号文本框添加了只读操作，不允许用户自行命名账号，由系统自动分配账号。

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-1.1a.jpg)

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-1.1b.jpg)

   

   （2）存款：a.允许用户输入账号及存款金额并进行存款，数据录入AccountInfo表中，存款信息直接显示在操作记录窗口。如果用户为活期存款用户，则直接进行结息操作，结息信息直接显示在操作记录窗口。
             b.对所有用户都设置了最低起存金额，在按下“确定”按钮后会对开户类型进行检测，低于相应的开户金额则弹出对话框进行提示。
             c.对定期存款用户及零存整取用户设置了设置存款期限的下拉框，数据会录入AccountInfo表中。
             d.对零存整取用户设置了设置零存整取固定金额的文本框，数据会录入AccountInfo表中。
             e.在用户输入账号后自动进行用户类型检测，如果为活期存款用户，则设置存款期限的下拉框和设置零存整取固定金额的文本框均设置为不可操作；如果为定期存款用户，则设置零存整取固定金额的文本框设置为不可操作。
             f. 在按下“确定”按钮后进行用户类型检测，如果为定期存款用户或零存整取用户，则检测该账户的已存款次数，定期存款仅允许一次存款；零存整取用户根据账户的存款期限分别为12次，36次，60次，如果检测到账户已存款次数已经超过了允许值，则弹出对话框进行提示。
             g. 在用户输入账号后自动进行用户类型检测，如果为零存整取用户，则检测其是否已进行过存款操作，如果监测到该账户已进行过一次以上存款，则设置存款期限的下拉框的文本属性设置为AccountInfo表中该账户之前设置的存款期限，设置零存整取固定金额的文本框的文本属性设置为AccountInfo表中该账户之前设置的的零存整取固定金额，并将下拉框和文本框设为不可操作。

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-1.2a.jpg)

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-1.2b.jpg)

   

   （3）取款：a. 允许用户输入账号密码，如果账号密码不匹配，则弹出对话框进行提示，如果账号密码不匹配则进行取款操作，数据录入AccountInfo表，取款信息直接显示在操作记录窗口中。
             b. 允许用户输入取款金额，如果取款金额大于账户余额或取款金额为负数，则弹出对话框进行提示。
             c.对于活期存款用户，结息操作已在存款结束后执行完毕。
             d.对于定期存款用户，在取款前进行结息操作，根据其取款时间与存款时间的时间差值与其之前设置的存款期限进行对比，按提前支取，满足期限，超出期限三种利息进行结息操作，数据录入AccountInfo表，结息信息直接显示在操作记录窗口中。
             e. 对于零存整取用户，在取款前进行结息操作，根据其取款时的存款次数与其之前设置的存款期限要求的次数进行对比，按违规，满足次数，超出次数三种利息进行结息操作，数据录入AccountInfo表，结息信息直接显示在操作记录窗口中。

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-1.3a.jpg)![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-1.3b.jpg)

   

   ##### 汇总查询模块

   主要实现的功能：
   （1）当日查询：a.按下“查询按钮”后，对当日发生的所有金额收入及支出情况进行显示。

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-2.1a.jpg)![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-2.1b.jpg)

   （2）汇总查询：a.分别设置了按账号，按操作日期，按操作类型，按操作涉及金额四个查询条件，允许对其中任意一个或多个条件组合实现多条件查询，按下“查询”按钮后，对满足条件的所有收入及支出情况进行显示。

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-2.2a.jpg)![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-2.2b.jpg)

   

   ##### 职员管理模块

   主要实现的功能：
   （1）职员管理：a.员工操作主页面对EmployeeInfo表中数据进行读取，并对员工的员工号，姓名，性别，工作日期，电话，身份证号码，照片，员工工资进行显示。

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-3.1a.jpg)![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-3.1b.jpg)

   ​              b.按下“添加”按钮后，跳转到添加员工信息页面，允许对其各项信息进行填写，按下“确定”按钮后，判断信息填写是否合法，若合法则将该员工添加到EmployeeInfo表中，并在员工操作主页面进行显示。

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-3.2a.jpg)![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-3.2b.jpg)

   ​              c.按下“修改”按钮后，跳转到修改员工信息界面，若未选中要修改的员工信息，则弹出对话框进行提示。在修改员工信息界面允许对其除员工工资外各项信息进行修改，按下“确定”按钮后，判断信息填写是否合法，若合法则将该员工添加到EmployeeInfo表中，并在员工操作主页面进行显示。

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-3.3a.jpg)![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-3.3b.jpg)

   ​              d.按下“删除”按钮后，弹出对话框询问是否进行删除操作，选择“Yes”则从EmployeeInfo表中移除该员工所有信息。若未选中要删除的员工信息，则弹出对话框进行提示。

   

   

   （2）调整工资：a.允许输入员工号，员工姓名，并选择工资调整方式及调整金额，对该员工的员工工资进行调整，若工资调整成功则弹出对话框显示员工调整后的员工工资，并将调整后信息录入EmployeeInfo表中。
                 b.设置了员工工资显示文本框，对该员工当前员工工资进行显示。
                 c.在输入员工号及员工姓名后，自动对其是否匹配进行验证，若不匹配，则弹出对话框进行提示。

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-3.4a.jpg)![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-3.4b.jpg)

   

   ##### 扩展功能模块

   主要实现的功能：
   （1）转账功能：a.允许两个活期存款用户间进行转账操作，数据录入AccountInfo表中，转账信息直接显示在操作记录窗口。
                 b.允许输入转账账户账号，转账方账户密码，收款方账户账号及转账金额，若转账方账户账号密码不匹配，则弹出对话框进行提示。
                 c.在按下“确定”按钮后，对用户转账金额进行检测，如果转账金额大于账户余额或转账金额为负数，则弹出对话框进行提示

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-4.1a.jpg)![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-4.1b.jpg)

   

   （2）抽奖及彩票功能：a.设置了抽奖与彩票的选择页面，允许用户进行选择。

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-4.2a.jpg)![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-4.2b.jpg)

   ​                   b.对于抽奖功能，允许活期存款账户进行抽奖，允许输入账户及密码，在按下“抽奖”按钮后，对用户类型及账户密码进行检测，如果不满足条件则弹出对话框进行提示。如满足条件，则进行一次抽奖，若中奖，则弹出对话框显示中奖金额，抽奖结果同样会在抽奖结果文本框中显示。

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-4.3a.jpg)![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-4.3b.jpg)

   ​                   c. 对于彩票功能，允许活期存款账户进行彩票购买，允许输入账户密码及自行选择的7位数字，在按下“确认”按钮后，对用户类型及账户密码进行检测，如果不满足条件则弹出对话框进行提示。如满足条件，则购买一次彩票，若中奖，则弹出对话框显示中奖金额，抽奖结果同样会在抽奖结果文本框中显示。

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-4.5a.jpg)![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-4.5b.jpg)

   ​                   d.设置了奖池账户，每次用户进行抽奖或彩票服务时扣除的费用均以转账形式由用户账户转入奖池账户，每次用户中奖后的奖金同样以转账形式由奖池账户转入用户账户。

   ##### 页面设计模块

   主要实现的功能：
   （1）登录界面的改进：a.允许用户输入账户密码，按下“登录”按钮后进行检测，若账户密码不匹配，则弹出对话框进行提示，并清空密码文本框。

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-0.0a.jpg)

   ​                   b.添加了验证码功能，允许账户输入，若验证码不匹配，则弹出对话框进行提示。验证码不区分大小写。若验证码不清晰可点击验证码进行刷新操作。

   ![img](https://treathy.com/wp-content/uploads/2023/12/bankmanager-0.0b.jpg)

   ​                   c.为登录按钮添加了“回车”默认。
   ​                   d.更改了登录页面背景。
   ​               
   （2）程序整体风格的设计：a.整体使用了Material Design in xaml 设计风格。
   ​                       b.在绝大部分控件前添加了相应的Icon矢量图标。
   ​                       c.更改了主界面背景。
   ​                       d.更改了主界面按钮的图片。

   #### 软件架构

   软件架构说明

   3.  基于VS2019和LocalDB编写

   #### 使用说明

   1.  通过BankManage/BankManage.sln启动主页面
   2.  详细功能演示参见项目演示视频：https://www.bilibili.com/video/BV1HM411k7tL/
